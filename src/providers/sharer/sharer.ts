import { Injectable } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Station } from '../interfaces';

@Injectable()
export class SharerProvider {
    constructor(private socialSharing: SocialSharing) { }

    share(station: Station): Promise<any> {
        let message = `
        Estación de servicio ${station['Rótulo']}\n
        Dirección: ${station['Dirección']}\n
        Horario: ${station['Horario']}\n
        Precios:\n
        `;

        for (const key of Object.keys(station)) {
            if (/^Precio/.test(key) && station[key] !== null) {
                message += `· ${key}: ${station[key]} €/l\n`
            }

        }
        message += ` `;

        let options = {
            message: message,
            subject: 'Te han compartido una estación de servicio'
        };

        return this.socialSharing.shareWithOptions(options);
    }
}