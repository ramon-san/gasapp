export interface Autonomy {
    IDCCAA: string;
    CCAA: string;
};

export interface Province extends Autonomy {
    IDPovincia: string;
    Provincia: string;
};

export interface Municipality extends Province {
    IDMunicipio: string;
    Municipio: string;
};

export interface Station extends Municipality {
    'IDEESS': string;
    'Latitud': string;
    'Longitud (WGS84)': string;
    '% BioEtanol': string;
    '% Éster metílico': string;
    'C.P.': string;
    'Dirección': string;
    'Horario': string;
    'Margen': string;
    'Precio Biodiesel': string | null;
    'Precio Bioetanol': string | null;
    'Precio Gas Natural Comprimido': string | null;
    'Precio Gas Natural Licuado': string | null;
    'Precio Gases licuados del petróleo': string | null;
    'Precio Gasoleo A': string | null;
    'Precio Gasoleo B': string | null;
    'Precio Gasolina 95 Protección': string | null;
    'Precio Gasolina 98': string | null;
    'Precio Nuevo Gasoleo A': string | null;
    'Remisión': string;
    'Rótulo': string;
    'Tipo Venta': string;
}

export interface User {
    name: string;
    email: string;
    favorites: Station[];
}