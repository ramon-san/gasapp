import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ComunidadesAutonomas, Provincias, Municipios, Estaciones } from '../apis';
import { Autonomy, Province, Municipality, Station } from '../interfaces';

@Injectable()
export class ZoneProvider {
    constructor(private http: HttpClient) { }

    getAutonomies(): Promise<Autonomy[]> {
        return new Promise((resolve, reject) => {
            this.http.get<Autonomy[]>(ComunidadesAutonomas).subscribe(response => {
                resolve(response);
            }, err => {
                console.error("Error occured: ", err);
                reject(err);
            });
        });
    }

    getProvinces(id: string): Promise<Province[]> {
        return new Promise((resolve, reject) => {
            this.http.get<Province[]>(Provincias + id).subscribe(response => {
                resolve(response);
            }, err => {
                console.error("Error occured: ", err);
                reject(err);
            });
        });
    }

    getMunicipalities(id: string): Promise<Municipality[]> {
        return new Promise((resolve, reject) => {
            this.http.get<Municipality[]>(Municipios + id).subscribe(response => {
                resolve(response);
            }, err => {
                console.error("Error occured: ", err);
                reject(err);
            });
        });
    }

    getStations(id: string): Promise<Station[]> {
        return new Promise((resolve, reject) => {
            this.http.get<any>(Estaciones + id).subscribe(response => {
                resolve(response.ListaEESSPrecio);
            }, err => {
                console.error("Error occured: ", err);
                reject(err);
            });
        });
    }
}