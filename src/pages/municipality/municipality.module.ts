import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MunicipalityPage } from './municipality';

@NgModule({
    declarations: [
        MunicipalityPage
    ],
    imports: [
        IonicPageModule.forChild(MunicipalityPage),
    ],
})
export class MunicipalityPageModule { }
