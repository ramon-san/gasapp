import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ZoneProvider } from '../../providers/zones/zones';
import { Municipality, Station, User } from '../../providers/interfaces';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, LatLng, Spherical, Geocoder, GeocoderResult, GeocoderRequest } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-municipality',
  templateUrl: 'municipality.html'
})
export class MunicipalityPage {
  map: GoogleMap;
  currentPosition: LatLng;
  municipality: Municipality;
  stations: Station[] = [];
  user: User;

  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public alertCtrl: AlertController,
    public zones: ZoneProvider
  ) {
    this.municipality = params.get('municipality');
    zones.getStations(this.municipality.IDMunicipio).then(stations => {
      this.stations = stations;
      this.user = JSON.parse(localStorage.getItem('user'));
      this.loadMap();
    });
  }

  loadMap() {
    let request: GeocoderRequest = { address: `${this.municipality.Municipio}` };
    Geocoder.geocode(request).then((response: GeocoderResult[]) => {

      let mapOptions: GoogleMapOptions = {
        camera: {
          target: response[0].position,
          zoom: 15,
          tilt: 30
        }
      };

      this.map = GoogleMaps.create('map_canvas', mapOptions);
      this.map.one(GoogleMapsEvent.MAP_READY)
        .then(() => {
          this.map.getMyLocation()
            .then(response => {
              this.currentPosition = response.latLng;

              this.map.addMarker({
                title: 'Mi posición',
                icon: 'blue',
                animation: 'DROP',
                position: response.latLng
              });

              for (const station of this.stations) {
                let stationPosition = {
                  lat: parseFloat(station['Latitud'].replace(',', '.')),
                  lng: parseFloat(station['Longitud (WGS84)'].replace(',', '.'))
                };

                let distance = Spherical.computeDistanceBetween(this.currentPosition, stationPosition);
                distance = distance / 1000;


                this.map.addMarker({
                  title: station['Rótulo'],
                  icon: { url: 'assets/icon/station.png' },
                  animation: 'DROP',
                  position: stationPosition
                })
                  .then(marker => {
                    marker.on(GoogleMapsEvent.MARKER_CLICK)
                      .subscribe(() => {

                        let message = `
                  <p>Dirección: ${station['Dirección']}</p>
                  <p>Horario: ${station['Horario']}</p>
                  <p>Distancia: ${distance.toFixed(2)} Km</p>
                  <p>Precios: </p>
                  <ul>`;

                        for (const key of Object.keys(station)) {
                          if (/^Precio/.test(key) && station[key] !== null) {
                            message += `<li>${key}: ${station[key]} €/l </li>`
                          }

                        }

                        message += `</ul>`;
                        let idx = this.user.favorites.findIndex(favStation => favStation.IDEESS === station.IDEESS);
                        let saveButton;
                        if (idx === -1) {
                          saveButton = {
                            text: 'Guardar',
                            handler: data => {
                              this.user.favorites.push(station);
                              localStorage.setItem('user', JSON.stringify(this.user));
                            }
                          };
                        } else {
                          saveButton = {
                            text: 'En Favoritos',
                            role: 'cancel',
                          }
                        }

                        let alert = this.alertCtrl.create({
                          title: station['Rótulo'],
                          message: message,
                          buttons: [
                            saveButton,
                            {
                              text: 'Cerrar',
                              role: 'cancel',
                            }
                          ]
                        });
                        alert.present();
                      });
                  });
              }
            });
        });
    });
  }
}
