import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutonomyPage } from './autonomy';

@NgModule({
    declarations: [
        AutonomyPage
    ],
    imports: [
        IonicPageModule.forChild(AutonomyPage),
    ],
})
export class AutonomyPageModule { }
