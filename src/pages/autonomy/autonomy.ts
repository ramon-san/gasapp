import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ZoneProvider } from '../../providers/zones/zones';
import { Autonomy, Province } from '../../providers/interfaces';

@IonicPage()
@Component({
  selector: 'page-autonomy',
  templateUrl: 'autonomy.html'
})
export class AutonomyPage {
  autonomy: Autonomy;
  provinces: Province[] = [];

  constructor(public navCtrl: NavController, public params: NavParams, public zones: ZoneProvider) {
    this.autonomy = params.get('autonomy');
    zones.getProvinces(this.autonomy.IDCCAA).then(provinces => this.provinces = provinces);
  }

}
