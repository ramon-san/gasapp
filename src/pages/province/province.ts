import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ZoneProvider } from '../../providers/zones/zones';
import { Province, Municipality } from '../../providers/interfaces';

@IonicPage()
@Component({
  selector: 'page-province',
  templateUrl: 'province.html'
})
export class ProvincePage {
  province: Province;
  municipalities: Municipality[] = [];

  constructor(public navCtrl: NavController, public params: NavParams, public zones: ZoneProvider) {
    this.province = params.get('province');
    zones.getMunicipalities(this.province.IDPovincia).then(municipalities => this.municipalities = municipalities);
  }

}
