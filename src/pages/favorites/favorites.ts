import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Station, User } from '../../providers/interfaces';
import { SharerProvider } from '../../providers/sharer/sharer';
import { Spherical, LatLng } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage {
  user: User;
  favorites: Station[] = [];
  currentPosition: LatLng;

  constructor(public navCtrl: NavController, public sharer: SharerProvider, private geolocation: Geolocation) { }

  ionViewWillEnter() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.favorites = this.user.favorites;
    this.geolocation.getCurrentPosition().then(response => {
      this.currentPosition = new LatLng(response.coords.latitude, response.coords.longitude);
    }).catch(error => {
      console.error('Error getting location', error);
    });
  }

  calcDistance(station: Station): string {
    if (this.currentPosition !== void 0) {
      let stationPosition = {
        lat: parseFloat(station['Latitud'].replace(',', '.')),
        lng: parseFloat(station['Longitud (WGS84)'].replace(',', '.'))
      };

      let distance = Spherical.computeDistanceBetween(this.currentPosition, stationPosition);
      distance = distance / 1000;
      return distance.toFixed(2);
    }

    return '-';
  }

  share(station: Station) {
    this.sharer.share(station);
  }

  remove(station: Station) {
    let idx = this.user.favorites.findIndex(favStation => favStation.IDEESS === station.IDEESS);
    if (idx > -1) {
      this.user.favorites.splice(idx, 1);
    }
    this.favorites = this.user.favorites;
  }

}
