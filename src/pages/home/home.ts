import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ZoneProvider } from '../../providers/zones/zones';
import { Autonomy } from '../../providers/interfaces';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  autonomies: Autonomy[] = [];

  constructor(public navCtrl: NavController, public zones: ZoneProvider) {
    zones.getAutonomies().then(autonomies => this.autonomies = autonomies);
  }

}
