import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { User } from '../../providers/interfaces';

@IonicPage()
@Component({
  selector: 'page-config',
  templateUrl: 'config.html'
})
export class ConfigPage {
  user: User;

  constructor(public navCtrl: NavController) {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  logout() {
    // La gestión de usuarios podría realizarse con una base de datos noSQL, guardando los favoritos en la base de datos.
    // Una función de login recuperaría éstos y los almacenaría en el localStorage.
    // Para cerrar la sesión borramos los dato del localStorage.
    localStorage.removeItem('user');
  }
}
