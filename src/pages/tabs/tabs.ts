import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab2Root = 'FavoritesPage';
  tab3Root = 'ConfigPage';

  constructor() {
    //*** For testing
    let user: any = localStorage.getItem('user');
    if (user === null) {
      user = {
        name: 'Tester',
        email: 'test@email.com',
        favorites: []
      };
      localStorage.setItem('user', JSON.stringify(user));
    }
    //***
  }
}
