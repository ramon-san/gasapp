import { Component } from '@angular/core';
import { Loading, LoadingController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

@Component({
  templateUrl: 'app.html'
})
export class GasApp {
  rootPage: string = 'TabsPage';
  offlineBlock: Loading;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    network: Network,
    loadingCtrl: LoadingController
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

    network.onDisconnect().subscribe(() => {
      this.offlineBlock = loadingCtrl.create({
        spinner: 'hide',
        content: 'Necesitas conexión a Internet para utilizar esta aplicación',
      });
      this.offlineBlock.present();
    });

    network.onConnect().subscribe(() => {
      this.offlineBlock.dismiss();
    });
  }
}
