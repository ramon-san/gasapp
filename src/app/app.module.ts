import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { GasApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ZoneProvider } from '../providers/zones/zones';
import { SharerProvider } from '../providers/sharer/sharer';

@NgModule({
  declarations: [
    GasApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(GasApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    GasApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    GoogleMaps,
    Geolocation,
    SocialSharing,
    ZoneProvider,
    SharerProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
